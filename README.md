# Finite.Commands [![MyGet][myget-image]][myget-link] [![Commits][ci-pipeline-image]][ci-pipeline-link] #

A simple, down-to-earth library for handling chat commands from various
instant-messaging services.

Feel free to join my Discord server: https://discord.gg/Y4d9ZWJ

## TODO ##

- Write full unit test suite
- Setup examples
    - Simple command-line interactive example
    - Simple discord bot example


[ci-pipeline-link]: https://gitlab.com/FiniteReality/Finite.Commands/pipelines
[ci-pipeline-image]: https://gitlab.com/FiniteReality/Finite.Commands/badges/master/pipeline.svg
[myget-link]: https://www.myget.org/feed/finitereality/package/nuget/Finite.Commands.Core
[myget-image]: https://img.shields.io/myget/finitereality/vpre/Finite.Commands.Core.svg?label=myget
